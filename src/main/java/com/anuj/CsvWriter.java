package com.anuj;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CsvWriter {

    final private Map<Integer, Integer> loanIdToFacilities;
    final private Map<Integer, Float> facilitiesIdToYields;

    public CsvWriter(Map<Integer, Integer> loanIdToFacilities, Map<Integer, Float> facilitiesIdToYields){
        this.facilitiesIdToYields = facilitiesIdToYields;
        this.loanIdToFacilities = loanIdToFacilities;
    }

    protected void printResultInCsv() throws IOException {
        List<String[]> allData = new ArrayList<>();
        for (Map.Entry<Integer, Integer> entry : loanIdToFacilities.entrySet()) {
            String[] data = new String[]{entry.getKey().toString(), entry.getValue().toString()};
            allData.add(data);
        }
        Affirm.class.getClassLoader().getResources("assignments_anuj.csv");

        writeDataAtOnce("src/main/resources/res/assignments_anuj.csv", new String[]{"loan_id", "facility_id"}, allData);

        allData = new ArrayList<>();
        for (Map.Entry<Integer, Float> entry : facilitiesIdToYields.entrySet()) {
            String[] data = new String[]{entry.getKey().toString(), String.valueOf(Math.round(entry.getValue()))};
            allData.add(data);
        }
        writeDataAtOnce("src/main/resources/res/yields_anuj.csv", new String[]{"facility_id", "expected_yield"}, allData);
    }

    /**
     * A simple write to write the result to the csv file exisiting in resource folder
     *
     * @param csv
     * @param header
     * @param allData
     * @throws IOException
     */
    private void writeDataAtOnce(String csv, String[] header, List<String[]> allData) throws IOException {

        FileWriter writer = new FileWriter(csv);
        writer.append(String.join(",", header));
        writer.append("\n");

        for (String[] rowData : allData) {
            writer.append(String.join(",", rowData));
            writer.append("\n");
        }

        writer.flush();
        writer.close();
    }
}
