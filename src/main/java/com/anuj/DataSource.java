package com.anuj;

import com.opencsv.bean.CsvToBeanBuilder;
import models.Covenants;
import models.Facilities;
import models.Loans;

import java.io.*;
import java.util.List;

public class DataSource {

    private static volatile DataSource dataSource = new DataSource();

    private static List<Covenants> covenants;
    private static List<Facilities> facilities;
    private static List<Loans> loans;

    //private constructor.
    private DataSource(){}

    public static DataSource getInstance(String testMode) throws IOException {

        covenants = new CsvToBeanBuilder<Covenants>(new FileReader("src/main/resources/"+testMode+"/covenants.csv"))
            .withType(Covenants.class)
            .build()
            .parse();

        facilities = new CsvToBeanBuilder<Facilities>(new FileReader("src/main/resources/"+testMode+"/facilities.csv"))
            .withType(Facilities.class)
            .build()
            .parse();

        loans = new CsvToBeanBuilder<Loans>(new FileReader("src/main/resources/"+testMode+"/loans.csv"))
            .withType(Loans.class)
            .build()
            .parse();

        return dataSource;
    }

    protected List<Covenants> getCovenants() {
        return covenants;
    }

    protected List<Facilities> getFacilities() {
        return facilities;
    }

    protected List<Loans> getLoans() {
        return loans;
    }
}
