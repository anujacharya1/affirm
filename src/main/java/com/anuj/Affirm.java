package com.anuj;

import models.Covenants;
import models.Facilities;
import models.Loans;

import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class Affirm {

  private static DataSource dataSource;

  public Affirm(DataSource ds) {
    dataSource = ds;
  }

  private void balanceLoanBook() throws IOException {

    final Map<Integer, Integer> loanIdToFacilities = new HashMap<>();
    final Map<Integer, Float> facilitiesIdToYields = new HashMap<>();

    // create map of facilties to covenants
    final Map<Integer, List<Covenants>> mapOfFacilitiesIdToCovenants = generateFacilitiesToCovenants();
    final Map<Integer, Facilities> facilitiesMap = new HashMap<>();

    for (Facilities facilitie : dataSource.getFacilities()) {
      facilitiesMap.put(facilitie.getId(), facilitie);
    }

    // stream loan one by one
    for (Loans loan : dataSource.getLoans()) {

      //assign loan to facilities based on the restriction
      for (Map.Entry<Integer, Facilities> entry : facilitiesMap.entrySet()) {

        Facilities fac = entry.getValue();

        //get list of Covenants for the facilitie
        final List<Covenants> covenantsList = mapOfFacilitiesIdToCovenants.get(fac.getId());

        boolean covenantsExistsForLoan = false;

        // check if any covenants exist for the loan
        for (Covenants covenants : covenantsList) {

          if ((covenants.getMaxDefaultLikelihood() != null
              && loan.getDefaultLikelihood() > covenants.getMaxDefaultLikelihood())
              || (covenants.getBannedState() != null && loan.getState()
              .equalsIgnoreCase(covenants.getBannedState()))) {
            covenantsExistsForLoan = true;
            break;
          }
        }

        // if none covenants exist we found the match and we can assignt he loan to that facilitie
        if (!covenantsExistsForLoan) {

          // found the match
          loanIdToFacilities.put(loan.getId(), fac.getId());

          // create map of facilities to loansIds
          final Float loanYield =
              (1 - loan.getDefaultLikelihood()) * loan.getInterestRate() * loan.getAmount()
                  - loan.getDefaultLikelihood() * loan.getAmount()
                  - fac.getInterestRate() * loan.getAmount();

          Float previousYield = facilitiesIdToYields.getOrDefault(fac.getId(), 0.0f);
          facilitiesIdToYields.put(fac.getId(), Float.sum(previousYield, loanYield));

          break;
        }
      }
    }
    CsvWriter csvWriter = new CsvWriter(loanIdToFacilities, facilitiesIdToYields);
    csvWriter.printResultInCsv();
  }

  /**
   * @return map of facelities_id to list of all covenants on this facelitie
   */
  private Map<Integer, List<Covenants>> generateFacilitiesToCovenants() {

    final Map<Integer, List<Covenants>> mapOfFacilitiesIdToCovenants = new HashMap<>();
    final Map<Integer, List<Facilities>> mapOfBankIdToFacilities = new HashMap<>();

    for (Facilities facilitie : dataSource.getFacilities()) {

      List<Facilities> facilitiesList = mapOfBankIdToFacilities
          .getOrDefault(facilitie.getBankId(), new LinkedList<>());
      facilitiesList.add(facilitie);
      mapOfBankIdToFacilities.put(facilitie.getBankId(), facilitiesList);
    }

    for (Covenants covenant : dataSource.getCovenants()) {

      final Set<Integer> facilitiesIds = new HashSet<>();

      // If facilitie id is null we need to apply this covenants is applicable on the bank level and all it's facilities
      if (covenant.getFacilityId() == null) {
        facilitiesIds.addAll(
            mapOfBankIdToFacilities.get(covenant.getBankId()).stream().map(Facilities::getId)
                .collect(Collectors.toList()));
      } else {
        facilitiesIds.add(covenant.getFacilityId());
      }

      for (Integer facelityId : facilitiesIds) {
        List<Covenants> covenantsList = mapOfFacilitiesIdToCovenants
            .getOrDefault(facelityId, new LinkedList<>());
        covenantsList.add(covenant);
        mapOfFacilitiesIdToCovenants.put(facelityId, covenantsList);
      }
    }

    return mapOfFacilitiesIdToCovenants;
  }

  /**
   * @param args can be small or large and it initialize all the file in that folder
   * @throws IOException
   */
  public static void main(String[] args) throws IOException {

    final Affirm affirm = new Affirm(DataSource.getInstance(args[0]));
    affirm.balanceLoanBook();

  }
}
