package models;

import com.opencsv.bean.CsvBindByName;

public class Facilities {

    @CsvBindByName(column = "amount")
    Float amount;

    @CsvBindByName(column = "interest_rate")
    Float interestRate;

    @CsvBindByName(column = "id")
    Integer id;

    @CsvBindByName(column = "bank_id")
    Integer bankId;

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public Float getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(Float interestRate) {
        this.interestRate = interestRate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBankId() {
        return bankId;
    }

    public void setBankId(Integer bankId) {
        this.bankId = bankId;
    }
}
