package models;

import com.opencsv.bean.CsvBindByName;
import com.sun.istack.internal.Nullable;

public class Covenants {

    @CsvBindByName(column = "facility_id")
    @Nullable
    Integer facilityId;

    @CsvBindByName(column = "max_default_likelihood")
    @Nullable
    Float maxDefaultLikelihood;

    @CsvBindByName(column = "bank_id")
    Integer bankId;

    @CsvBindByName(column = "banned_state")
    @Nullable
    String bannedState;

    public Integer getFacilityId() {
        return facilityId;
    }

    public void setFacilityId(Integer facilityId) {
        this.facilityId = facilityId;
    }

    public Float getMaxDefaultLikelihood() {
        return maxDefaultLikelihood;
    }

    public void setMaxDefaultLikelihood(Float maxDefaultLikelihood) {
        this.maxDefaultLikelihood = maxDefaultLikelihood;
    }

    public Integer getBankId() {
        return bankId;
    }

    public void setBankId(Integer bankId) {
        this.bankId = bankId;
    }

    public String getBannedState() {
        return bannedState;
    }

    public void setBannedState(String bannedState) {
        this.bannedState = bannedState;
    }
}
