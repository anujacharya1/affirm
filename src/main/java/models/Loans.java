package models;

import com.opencsv.bean.CsvBindByName;

public class Loans {

    @CsvBindByName(column = "interest_rate")
    Float interestRate;

    @CsvBindByName(column = "amount")
    Integer amount;

    @CsvBindByName(column = "id")
    Integer id;

    @CsvBindByName(column = "default_likelihood")
    Float defaultLikelihood;

    @CsvBindByName(column = "state")
    String state;

    public Float getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(Float interestRate) {
        this.interestRate = interestRate;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Float getDefaultLikelihood() {
        return defaultLikelihood;
    }

    public void setDefaultLikelihood(Float defaultLikelihood) {
        this.defaultLikelihood = defaultLikelihood;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
