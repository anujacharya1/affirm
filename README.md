
1. How long did you spend working on the problem? What did you find to be the most difficult part?
   - Roughly 4-5 hours. Understanding the question and acceptance criteria

2. How would you modify your data model or code to account for an eventual introduction of new, as-of-yet unknown types of covenants, beyond just maximum default likelihood and state restrictions?
    - Current it is part of the application process of matching. But if there are more covenants it need to be in it's own service layer just like how i have partition data layer
    
3. How would you architect your solution as a production service wherein new facilities can be introduced at arbitrary points in time. Assume these facilities become available by the finance team emailing your team and describing the addition with a new set of CSVs.
    - All the addition should be part of it configuration and when new facilities are introduced with new CSV it would be loaded into the production instance and the server will take care of it during intialization of service
    
4. Your solution most likely simulates the streaming process by directly calling a method in your code to process the loans inside of a for loop. What would a REST API look like for this same service? Stakeholders using the API will need, at a minimum, to be able to request a loan be assigned to a facility, and read the funding status of a loan, as well as query the capacities remaining in facilities.
    
    `<ip>/affirm/loans/ POST`
    This will make the call to the service layer and the business logic to find the assoicated facilities
    
    `<ip>/affirm/loans/<id> GET`
    This can return the funding status of the loan
    
    `<ip>/affirm/facilities/<id> GET`
    This can return the facilities status by id and it's capacity

5. How might you improve your assignment algorithm if you were permitted to assign loans in batch rather than streaming? We are not looking for code here, but pseudo code or description of a revised algorithm appreciated.
    - Will sort the facilities by its highest over all yields and process the loan accordingly. Will batch loan for 24 hours window and based on the higest yield will assign the loan rather than randomly which is happening right now
    
6. Discuss your solution’s runtime complexity.
    - The runtime complexity is O(facilites)*O(covenants) as for each loan we need to check each facilites and every covenants to make sure there are no filters in place
